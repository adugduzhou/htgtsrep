#!/usr/bin/env python3
import os
import sys
import logging

def Vstat(filename, V_annolist, readtype):
    # parse the input processed igblast result file
    Vcounts = {}
    for V in V_annolist:
        Vcounts[V] = []
    for line in open(filename):
        if line.startswith('Qname'): continue
        l = line.strip().split('\t')
        if l[35] == 'failed': continue
        if readtype == 'join' and l[34] == 'R1':continue
        if l[36] not in Vcounts:
            Vcounts[l[36]] = [l[5]]
        else:
            Vcounts[l[36]].append(l[5])
    return Vcounts

def Vseg_stat(args, readtype, deduptype):
    # load annotation file
    V_anno = {}
    V_annolist = []
    V_alllist = []
    annofile = ''
    if args.Vannotation:
        annofile = args.Vannotation
    elif args.organism == 'mouse':
        annofile = '%s/database/annotation/mouse_%s_anno.txt' % (scriptdir, args.VDJdatabase)
        if args.mousestrain:
            annofile = '%s/database/annotation/mouse_%s_B6_anno.txt' % (scriptdir, args.VDJdatabase)
    if annofile:
        for line in open(annofile):
            l = line.strip().split('\t')
            try:
                V_annolist.append(l[0])
                V_alllist.append(l[0])
                V_anno[l[0]] = [l[1], l[2]]
            except:
                logging.error('Cannot parse annotation file correctly')
    # stat V segments
    sample_vstat = {}
    for sample in sample_info:
        eachdir = '%s/%s' % (args.outdir, sample)
        if deduptype == 'pass':
            Vcounts = Vstat('%s/%s.pass.xls' % (eachdir, sample), V_annolist, readtype)
        if deduptype == 'dedup':
            Vcounts = Vstat('%s/%s.dedup.xls' % (eachdir, sample), V_annolist, readtype)
        sample_vstat[sample] = Vcounts

        if deduptype == 'pass':
            fv = open('%s/%s.Vstat.%s.xls' % (eachdir, sample, readtype), 'w')
        if deduptype == 'dedup':
            fv = open('%s/%s.Vstat.%s.dedup.xls' % (eachdir, sample, readtype), 'w')
        fv.write('Name\tCoordinates\tFunction\tIn-frame\tnon-translational\tTotal\n')
        for V in V_annolist:
            inframeNum = Vcounts[V].count('In-frame')
            outframeNum = Vcounts[V].count('Out-of-frame')
            stopframeNum = Vcounts[V].count('In-frame with stop codon')
            fv.write('%s\t%s\t%s\t%d\t%d\t%d\n' % (V, V_anno[V][0], V_anno[V][1], inframeNum, outframeNum + stopframeNum, len(Vcounts[V])))
        for V in Vcounts:
            if V in V_annolist: continue
            if V not in V_alllist: V_alllist.append(V)
            inframeNum = Vcounts[V].count('In-frame')
            outframeNum = Vcounts[V].count('Out-of-frame')
            stopframeNum = Vcounts[V].count('In-frame with stop codon')
            fv.write('%s\t-\t-\t%d\t%d\t%d\n' % (V, inframeNum, outframeNum + stopframeNum, len(Vcounts[V])))
        fv.close()

    # compile all V stat
    if deduptype == 'pass':
        fall = open('%s/allsample.Vstat.%s.xls' % (args.outdir, readtype), 'w')
    if deduptype == 'dedup':
        fall = open('%s/allsample.Vstat.%s.dedup.xls' % (args.outdir, readtype), 'w')
    title = 'Name\tCoordinates\tFunction'
    for sample in sample_info:
        title += '\t%s_In-frame\t%s_No-translational\t%s_Total' % (sample, sample, sample)
    fall.write('%s\n' % title)

    for V in V_alllist:
        if V in V_annolist:
            data = '%s\t%s\t%s' % (V, V_anno[V][0], V_anno[V][1])
        else:
            data = '%s\t-\t-' % V
        for sample in sample_info:
            if V not in sample_vstat[sample]:
                inframeNum = 0
                notransNum = 0
            else:
                inframeNum = sample_vstat[sample][V].count('In-frame')
                notransNum = sample_vstat[sample][V].count('Out-of-frame') + sample_vstat[sample][V].count('In-frame with stop codon')
            totalNum = inframeNum + notransNum
            data += '\t%d\t%d\t%d' % (inframeNum, notransNum, totalNum)
        fall.write('%s\n' % data)
    fall.close()

def clean_up(args):
    # clean up
    for sample in sample_info:
        eachdir = '%s/%s' % (args.outdir, sample)
        if not os.path.exists('%s/reads_fasta' % eachdir):
            os.system('mkdir {0}/reads_fasta {0}/reads_fastq {0}/igblast_results {0}/igblast_raw {0}/bowtie_sam'.format(eachdir))
        os.system('mv %s/*fa %s/reads_fasta' % (eachdir,eachdir))
        os.system('mv %s/*fq %s/*list %s/reads_fastq' % (eachdir, eachdir, eachdir))
        os.system('mv %s/*igblast %s/igblast_raw' % (eachdir, eachdir))
        os.system('mv %s/*join*all* %s/igblast_results' % (eachdir, eachdir))
        if args.genomealign == 'T':
            os.system('mv %s/*.sam %s/bowtie_sam' % (eachdir, eachdir))
        os.system('gzip %s/reads_fast*/*' % (eachdir))

def get_statistic(si, args):
    global sample_info
    global scriptdir
    sample_info = si
    scriptdir = os.path.dirname(os.path.realpath(__file__)).replace('scripts', '')

    logging.info('Wrap up results and clean up......')

    Vseg_stat(args, 'join', 'pass')
    Vseg_stat(args, 'join', 'dedup')
    Vseg_stat(args, 'pass', 'pass')
    Vseg_stat(args, 'pass', 'dedup')

    clean_up(args)

    logging.info('All done!!! Have a good day, Bye~ :)')
