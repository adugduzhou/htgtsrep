#!/usr/bin/env python3
import os
import sys
from scripts.rep_lib import *
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna

class mutProfile:
    ''' read pipeline log for igblast paramters and generate mut profile for each VDJ gene'''

    def __init__(self, si, args):
        self.si = si
        self.args = args
        self.param_val = {}
        self.VDJ_seq = {}   # genename: sequence
        self.V_CDR = {}    # V name: [CDR1 2 3 start, end]
        self.scriptdir = os.path.dirname(os.path.realpath(__file__)).replace('scripts', '')

        # load param values
        self.param_val['organism'] = 'mouse'
        self.param_val['database'] = 'IGH'
        for line in open('%s/pipeline.log' % self.args.dir):
            if line.find('igblastn') != -1 and line.find('ERROR') == -1:
                igblast_param = ' -germline_db_V ' + line.split('germline_db_V')[1].split('-outfmt')[0]
                igblast_param = igblast_param + '-show_translation '
                self.param_val['igblast'] = igblast_param
            if line.find('HTGTSrep.py') != -1 and line.find('VDJdatabase') != -1:
                self.param_val['database'] = line.split('VDJdatabase')[1].strip().split()[0]
            if line.find('--organism human') != -1:
                self.param_val['organism'] = 'human'
        if args.dedup == 'T':
            self.param_val['dtype'] = "dedup"
        else:
            self.param_val['dtype'] = "pass"

        # load VDJ gene sequences
        if self.args.geneseq:
            seqfile = self.args.geneseq
        else:
            seqfile = '%s/database/imgt_seq/%s_%s_imgt' % (self.scriptdir, self.param_val['organism'], self.param_val['database'])
        for line in open(seqfile):
            l = line.strip().split('\t')
            self.VDJ_seq[l[0]] = l[1].upper()
        if len(self.VDJ_seq) == 0:
            logging.error('Cannot find any VDJ gene sequence.')

        # load V CDR info
        if self.args.genecdr:
            cdrfile = self.args.genecdr
        else:
            cdrfile = '%s/database/imgt_seq/%s_%s_CDR.txt' % (self.scriptdir, self.param_val['organism'], self.param_val['database'])
        for line in open(cdrfile):
            l = line.strip().split('\t')
            self.V_CDR[l[0]] = l[1:]

        self.runProfile()

    def runProfile(self):
        # run profiling for each sample
        dtype = self.param_val['dtype']
        for sample in self.si:
            eachdir = '%s/%s' % (self.args.dir, sample)

            # Load pass or dedup file
            read_info = {}   # read: [info]
            VDJ_reads = {}   # VDJgene: [read list]
            VDJ_mutNum = {}  # VDJgene: [sum mutation number]
            pfile = '%s/%s.%s.xls' % (eachdir, sample, dtype)
            try:
                exec_log("perl -pi -e 's/\\r\\n|\\n|\\r/\\n/g' %s" % (pfile))
            except:
                logging.error('Problem converting line feeds to unix format')
                sys.exit(-1)
            for line in open(pfile, 'rU'):
                l = line.strip().split('\t')
                if line.startswith('Qname'): continue
                if self.args.joinonly == 'T' and l[-4] != 'join': continue
                read_info[l[0]] = l
                # only interested in V gene, only select column 2
                for item in l[1:2]:
                    if item not in self.VDJ_seq:
                        continue
                    if item not in VDJ_reads:
                        VDJ_reads[item] = []
                        VDJ_mutNum[item] = 0
                    # if use joined reads, need to check V coverage
                    if self.args.joinonly == 'T':
                        if int(l[13])/float(len(self.VDJ_seq[item])) >= self.args.min_Vcov:
                            VDJ_reads[item].append(l[0])
                    else:
                        VDJ_reads[item].append(l[0])
                    # count mutation number
                    try:
                        VDJ_mutNum[item] += int(l[14])
                    except:
                        VDJ_mutNum[item] += 0
            if len(VDJ_reads) == 0:
                logging.warnings('The gene sequence file might be wrong, please check!')
            # perform igblast search with fmt 3
            rfile = '%s/mutProfile/readlist.txt' % eachdir
            if self.args.joinonly == 'T':
                exec_log("grep -v Qname %s | grep join | awk '{print $1}' > %s" % (pfile, rfile))
            else:
                exec_log("grep -v Qname %s | awk '{print $1}' > %s" % (pfile, rfile))

            # generate a V coverage report
            fcov = open('%s/mutProfile/ratio.tmp' % eachdir, 'w')
            read_ratio = {}
            Vfamily_Vgene = {}
            for read in read_info:
                l = read_info[read]
                Vfamily_Vgene[l[-2]] = 1
                ratio = float(l[13])/len(self.VDJ_seq[l[1]])
                if ratio > 1: ratio = 1
                read_ratio[read] = ratio
                fcov.write('%s\t%s\t%s\t%d\t%.3f\t%s\n' % (l[0], l[1], l[13], len(self.VDJ_seq[l[1]]), ratio, l[5]))
            fcov.close()
            os.system('sort -n -k5,5 %s/mutProfile/ratio.tmp > %s/mutProfile/%s.Vcov.xls' % (eachdir, eachdir, sample))
            os.system('rm -rf %s/mutProfile/ratio.tmp' % eachdir)

            self.Vfam_mutstat = {'NP': {}, 'P': {}}   # 'NP': {Vfam: [total_mutate_num, total_align_V]}  of productive
            # generate a mutation stat file
            fmfrq = open('%s/mutProfile/%s.Mutfreq.xls' % (eachdir, sample), 'w')
            fmfrq.write('V gene\tposition\tFunction\tV mut num\tTotal V gene length\tmut freq % (per read per bp)\tIn-frame\tnon-translational\tTotal(joined reads only)\tTotal Mut freq % (per bp)\n')
            total_ratio = 100 * sum([int(read_info[r][14]) for r in read_info]) / sum([len(self.VDJ_seq[read_info[r][1]]) for r in read_info])
            for line in open('%s/%s.Vstat.pass.xls' % (eachdir, sample)):
                V_mut = 0
                V_len = 0
                inframeNum = 0
                nonproduct = 0
                l = line.strip().split()
                if line.startswith('Name'): continue
                if l[0] in Vfamily_Vgene:
                    self.Vfam_mutstat['P'][l[0]] = [0, 0, 0]
                    self.Vfam_mutstat['NP'][l[0]] = [0, 0, 0]
                    for r in read_info:
                        info = read_info[r]
                        if read_ratio[r] >= self.args.min_Vcov and info[-2] == l[0]:
                            V_mut += int(info[14])
                            V_len += len(self.VDJ_seq[info[1]])
                            if info[5] == 'In-frame':
                                inframeNum += 1
                            if info[5] != 'In-frame': nonproduct += 1
                            inframeinfo = '%d\t%d\t%d' % (inframeNum, nonproduct, nonproduct + inframeNum)
                    if V_len > 0:
                        fmfrq.write('%s\t%s\t%s\t%d\t%d\t%.6f\t%s\t%f\n' % (
                                l[0], l[1], l[2], V_mut, V_len, 100*(V_mut/V_len),
                                inframeinfo, total_ratio))
                    else:
                        fmfrq.write('%s\t%s\t%s\t0\t0\t0\t0\t0\t0\t%f\n' % (l[0], l[1], l[2], total_ratio))
                else:
                    fmfrq.write('%s\t%s\t%s\t0\t0\t0\t0\t0\t0\t%f\n' % (l[0], l[1], l[2], total_ratio))
            fmfrq.close()

            if self.args.joinonly == 'T':
                subset = 'join'
            else:
                subset = 'join_R2'
                exec_log('cat {0}/reads_fastq/{1}_join.fq.gz {0}/reads_fastq/{1}_unjoinR2.fq.gz > {0}/reads_fastq/{1}_join_R2.fq.gz'.format(eachdir, sample))
            readfile = '%s/reads_fastq/%s_%s.fq.gz' % (eachdir, sample, subset)

            if line_count('%s' % rfile) == 0:
                logging.error('No read in the %s.' % rfile)
                sys.exit(-1)
            exec_log("seqtk subseq %s %s | fastq_quality_filter -q %d -p %d | fastq_masker -q %d -Q33 | fastq_to_fasta -Q33 -n -o %s/mutProfile/%s_%s.%s.fa" % (
                    readfile, rfile, self.args.qscore, self.args.qscore_cov, self.args.qscore, eachdir, sample, subset, dtype))
            # run igblast3 and parsing
            igblast3file = '%s/mutProfile/%s_%s.%s.igblast3' % (eachdir, sample, subset, dtype)
            cmdline = '%s/igblast_bin/igblastn -query %s/mutProfile/%s_%s.%s.fa -organism %s' % (self.scriptdir, eachdir, sample, subset, dtype, self.param_val['organism'])
            cmdline = cmdline + self.param_val['igblast'] + ' -outfmt 3 -out %s' % (igblast3file)
            os.environ['IGDATA'] = '%s/database/' % self.scriptdir
            os.environ['BLASTDB'] = '%s/database/' % self.scriptdir
            if self.args.mutreport == 'F':
                exec_log(cmdline)
            self.parse_igblast3(igblast3file, read_info, VDJ_reads, sample, "NP")
            self.parse_igblast3(igblast3file, read_info, VDJ_reads, sample, "P")
            self.mutstat(eachdir, read_info, VDJ_reads, sample)

    def mutstat(self, eachdir, read_info, VDJ_reads, sample):
        fout = open('%s/mutProfile/%s.Mutfreq.adjust.xls' % (eachdir, sample), 'w')
        fout.write('V gene\tposition\tFunction\tIn-frame_mut\tIn-frame_V_match\tIn-frame_%mut\tIn-frame_reads\tNP_mut\tNP_V_match\tNP_%mut\tNP_reads\tadjust_mut\tadjust_V_match\tadjust_%mut\tadjust_reads\ttotal_P_%mut\ttotal_NP_%mut\ttotal_all_%mut\n')
        total_NP_mut = sum([self.Vfam_mutstat['NP'][gene][0] for gene in self.Vfam_mutstat['NP']])
        total_P_mut = sum([self.Vfam_mutstat['P'][gene][0] for gene in self.Vfam_mutstat['P']])
        total_NP_len = sum([self.Vfam_mutstat['NP'][gene][1] for gene in self.Vfam_mutstat['NP']])
        total_P_len = sum([self.Vfam_mutstat['P'][gene][1] for gene in self.Vfam_mutstat['P']])
        total_NP_ratio = total_NP_mut / total_NP_len * 100
        total_P_ratio = total_P_mut / total_P_len * 100
        total_ratio = (total_NP_mut + total_P_mut) / (total_NP_len + total_P_len) * 100
        for line in open('%s/mutProfile/%s.Mutfreq.xls' % (eachdir, sample)):
            if line.startswith('V'): continue
            l = line.strip().split('\t')
            if l[0] not in self.Vfam_mutstat['P']:
                fout.write('%s\t%s\t%s\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t%.6f\t%.6f\t%.6f\n' % (
                            l[0], l[1], l[2], total_P_ratio, total_NP_ratio, total_ratio))
            else:
                gene = l[0]
                P_ratio = NP_ratio = ratio = 0
                P_mut = self.Vfam_mutstat['P'][gene][0]
                P_len = self.Vfam_mutstat['P'][gene][1]
                if P_len > 0: P_ratio = P_mut / P_len * 100
                NP_mut = self.Vfam_mutstat['NP'][gene][0]
                NP_len = self.Vfam_mutstat['NP'][gene][1]
                if NP_len > 0: NP_ratio = NP_mut / NP_len * 100
                mut = P_mut + NP_mut
                length = P_len + NP_len
                if length > 0: ratio = mut / length * 100
                fout.write('%s\t%s\t%s\t%d\t%d\t%.6f\t%d\t%d\t%d\t%.6f\t%d\t%d\t%d\t%.6f\t%d\t%.6f\t%.6f\t%.6f\n' % (
                l[0], l[1], l[2], P_mut, P_len, P_ratio, self.Vfam_mutstat['P'][gene][2],
                NP_mut, NP_len, NP_ratio, self.Vfam_mutstat['NP'][gene][2],
                mut, length, ratio, self.Vfam_mutstat['P'][gene][2] + self.Vfam_mutstat['NP'][gene][2],
                total_P_ratio, total_NP_ratio, total_ratio))
        fout.close()

    def parse_igblast3(self, igblast3file, read_info, VDJ_reads, sample, Ptype):
        # parse igblast3 file and create mutation table
        nucCovnum = {'A': 0, 'T': 0, 'C': 0, 'G': 0}
        nucMutnum = {'A': 0, 'T': 0, 'C': 0, 'G': 0}
        genetotalseq = ''
        statfilelist = {}
        current_read = ''
        read_Ptype = {}
        for read in read_info:
            if read_info[read][5] == 'In-frame':
                if Ptype == 'P': read_Ptype[read] = 1
            else:
                if Ptype == 'NP': read_Ptype[read] = 1
        # read: {pos: nucl or gap in alignment}
        nucl_seq = {}
        gap_seq = {}
        CDRprocesslist = {}
        for line in open(igblast3file):
            if line.startswith('Query='):
                current_read = line.replace('Query=', '').strip()
                current_queryseq = ''
                # init variable
                if current_read in read_Ptype:
                    nucl_seq[current_read] = {}
                    gap_seq[current_read] = {}
            elif current_read in read_Ptype:
                l = line.strip().split()
                if line.strip().startswith(('lcl|Query_', 'Query_')):
                    current_queryseq = l[2]
                if line.startswith(('V  ')): #, 'D  ', 'J  ')):
                    if l[3] not in VDJ_reads: continue
                    if current_read not in VDJ_reads[l[3]]: continue
                    if current_read not in nucl_seq:
                        nucl_seq[current_read] = {}
                    #if current_read not in CDRprocesslist:
                    #    CDRprocesslist[current_read] = 1
                    #    self.CDR_cal(int(l[4]), current_CDR, l[3])    # put CDR info to self.V_CDR
                    if l[3] not in nucl_seq[current_read]:
                        nucl_seq[current_read][l[3]] = {}
                    if current_read not in gap_seq:
                        gap_seq[current_read] = {}
                    if l[3] not in gap_seq[current_read]:
                        gap_seq[current_read][l[3]] = []
                    startpos = int(l[4])
                    endpos = int(l[6])
                    start_unmatch = len(l[5]) - len(l[5].lstrip('-'))
                    for nuc in l[5].strip('-'):
                        if nuc != '-':
                            if nuc == ".":
                                nucl_seq[current_read][l[3]][startpos] = nuc
                            else:
                                nucl_seq[current_read][l[3]][startpos] = current_queryseq[start_unmatch]
                            startpos += 1
                        else:
                            gap_seq[current_read][l[3]].append('%d|%s' % (startpos, current_queryseq[start_unmatch]))
                        start_unmatch += 1
        # output read profile for each V,D,J gene
        for gene in VDJ_reads:
            gseq = self.VDJ_seq[gene]
            genetotalseq += gseq

            pos_rnum = {}
            pos_mnum = {}
            pos_mtype = {}

            for i in range(1, len(gseq)+1):
                pos_rnum[i] = 0
                pos_mnum[i] = 0
                pos_mtype[i] = {'A': 0, 'T': 0, 'C': 0, 'G': 0}

            dirname = '%s/nucfile' % os.path.dirname(igblast3file)
            if not os.path.exists(dirname):
                os.system('mkdir %s' % dirname)
                os.system('mkdir %s' % dirname.replace('nucfile', 'seqlogo'))
                os.system('mkdir %s' % dirname.replace('nucfile', 'mutsfile'))
                os.system('mkdir %s' % dirname.replace('nucfile', 'readsfile'))
                os.system('mkdir %s' % dirname.replace('nucfile', 'clones'))
            nucfilename = '%s/%s.%s.%s.nuc.txt' % (dirname, sample, gene.replace('/', '-').replace('*', '-').replace('(', '').replace(')', ''), Ptype)
            fout = open(nucfilename, 'w')

            title = ['ID'] + [t for t in gseq]
            fout.write('%s\n' % '\t'.join(title))
            for read in nucl_seq:
                if gene not in nucl_seq[read]: continue
                align = []
                for i in range(1, len(gseq)+1):
                    symbol = nucl_seq[read][gene].get(i, "-")
                    align.append(symbol)
                    if symbol == '.':
                        pos_rnum[i] += 1
                        nucCovnum[gseq[i-1]] += 1
                    if symbol in 'ATCG':
                        pos_rnum[i] += 1
                        pos_mnum[i] += 1
                        pos_mtype[i][symbol] += 1
                        nucCovnum[gseq[i-1]] += 1
                        nucMutnum[gseq[i-1]] += 1
                fout.write('%s\t%s\n' % (read, '\t'.join(align)))
            fout.close()

            fmut = open(nucfilename.replace('nucfile', 'mutsfile').replace('nuc', 'muts'), 'w')
            fread = open(nucfilename.replace('nucfile', 'readsfile').replace('nuc', 'reads'), 'w')
            fclone = open(nucfilename.replace('nucfile', 'clones').replace('nuc', 'clones'), 'w')
            fclone.write('Expt\tRead\tBp\tCoords\tDup\tfiltsubs\tfiltdels\tfiltins\tfiltdelbp\tfiltdelsize\tSubs\tDels\tDelBp\tLargeDel\tIns\tInsBp\tLargeIns\n')
            realseq = []
            for line in open(nucfilename):
                l = line.strip().split()
                if line.startswith('ID'):
                    realseq = l
                else:
                    subpos = []
                    readseq = (''.join(l[1:])).strip('-')
                    readseq_rmleft = (''.join(l[1:])).lstrip('-')
                    vfam = read_info[l[0]][-2]
                    self.Vfam_mutstat[Ptype][vfam][1] += len(readseq)
                    self.Vfam_mutstat[Ptype][vfam][2] += 1
                    for i in range(1, len(l)):
                        if l[i] in 'ATCG':
                            subpos.append(i)
                            fmut.write('%s\t%s\t%d\tsub\t%s\t%s\n' % (sample, l[0], i, realseq[i], l[i]))
                            self.Vfam_mutstat[Ptype][vfam][0] += 1
                    leftpos = len(l) - len(readseq_rmleft)
                    rightpos = leftpos + len(readseq) - 1
                    poslist = [leftpos]
                    for sub in subpos:
                        poslist.append(sub-1)
                        poslist.append(sub+1)
                    poslist.append(rightpos)
                    segments = []
                    for j in range(0, int(len(poslist)/2)):
                        segments.append('%d-%d' % (poslist[j*2], poslist[j*2+1]))
                    fread.write('%s\t%s\t%d\t%s\n' % (sample, l[0], len(readseq), ','.join(segments)))
                    fclone.write('%s\t%s\t%d\t%s\t0\t%d\t0\t0\t0\t\t%d\t0\t0\t0\t0\t0\t0\n' % ( sample, l[0], len(readseq), ','.join(segments), len(segments)-1, len(segments)-1 ))
            fmut.close()
            fread.close()
            fclone.close()

            statfile = '%s/%s.%s.stat.%s.txt' % (dirname, sample, gene.replace('/', '-').replace('*', '_').replace('(', '').replace(')', ''), Ptype)
            statfilelist[gene] = statfile
            fstat = open(statfile, 'w')
            fstat.write('Pos\tMut\tTotal\tBase\tY\tA\tT\tC\tG\n')
            for pos in range(1, len(gseq)+1):
                mut_rate = 0
                if pos_rnum[pos] > 0:
                    mut_rate = float(pos_mnum[pos])/float(pos_rnum[pos])
                fstat.write('%d\t%d\t%d\t%s\t%f\t%d\t%d\t%d\t%d\n' % \
                (pos, pos_mnum[pos], pos_rnum[pos], gseq[pos-1], mut_rate,
                pos_mtype[pos]['A'], pos_mtype[pos]['T'],
                pos_mtype[pos]['C'], pos_mtype[pos]['G']))
            fstat.close()

            # run R scripts
            profilepath = '%s/profiles' % os.path.dirname(igblast3file)
            if not os.path.exists(profilepath):
                os.system('mkdir %s' % profilepath)
            if file_len(nucfilename) >= self.args.min_readmap and self.args.mutreport == 'F':
                cdr = self.V_CDR[gene]
                cdrset = 'cdr1_start=%s cdr1_end=%s cdr2_start=%s cdr2_end=%s cdr3_start=%s cdr3_end=%s' % (
                        cdr[0], cdr[1], cdr[2], cdr[3], cdr[4], cdr[5])
                exec_log('Rscript %s/scripts/SHMPlot2.R %s %s plotrows=1 figureheight=2 showsequence=FALSE ymax=%f %s ' % (
                        self.scriptdir, statfile, statfile.replace('nucfile', 'profiles').replace('.txt', '.pdf'), self.args.ymax_DNA, cdrset))
                self.proteinprofile(nucfilename)

    def proteinprofile(self, nucfilename):
        AA = ''
        protfilename = nucfilename.replace('nuc.', 'protein.')
        fpro = open(protfilename, 'w')
        for line in open(nucfilename):
            l = line.strip().split()
            if line.startswith('ID'):
                AA = ''.join(l[1:])
                continue
            fpro.write('>%s\n' % l[0])
            translateseq = ''
            for t in range(0, int((len(l)-1)/3)-1):
                nuctriple = ''
                if l[3 * t + 1] == '.':
                    nuctriple += AA[3 * t]
                else:
                    nuctriple += l[3 * t + 1]
                if l[3 * t + 2] == '.':
                    nuctriple += AA[3 * t + 1]
                else:
                    nuctriple += l[3 * t + 2]
                if l[3 * t + 3] == '.':
                    nuctriple += AA[3 * t + 2]
                else:
                    nuctriple += l[3 * t + 3]
                if '-' in nuctriple or 'N' in nuctriple:
                    translateseq += '-'
                else:
                    translateseq += str(Seq(nuctriple, generic_dna).translate())
            fpro.write('%s\n' % translateseq)
        fpro.close()
        os.system('weblogo -f %s -o %s -D fasta --units probability -A protein --composition none --size large -n 100 --scale-width NO --color-scheme chemistry -S %f' %
                    (protfilename, protfilename.replace('txt', 'eps').replace('nucfile', 'seqlogo'), self.args.ymax_protein))
