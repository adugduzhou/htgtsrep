#!/usr/bin/env python3
import os
import sys
import logging
import multiprocessing
from scripts.rep_lib import *
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna

def hit_filter(topinfo, args):
    if args.J_gene:
        if not topinfo[3].startswith(args.J_gene):        # 1. check J gene if specified
            return 'Not_match_J_gene'

    if topinfo[17] == '-':
        return 'No_V_alignment_score'

    if args.J_length:
        try:
            int(topinfo[15])
        except:
            return 'No_J_length'
        if args.J_length > int(topinfo[15]):                   # 2. check J length if specified
            return 'Shorter_than_min_J_length'

    try:
        float(topinfo[17])
    except:
        return 'Tophit1_has_no_score'
    if float(topinfo[17]) < args.V_score:          # 6. top hit score higher than cutoff (default 150)
        return 'Tophit_score_lower_than_cutoff'

    if '-' in [topinfo[13], topinfo[14]]:
        return 'No_V_alignment_info'

    try:
        int(topinfo[13])
    except:
        return 'No_V_alignment_length_info'
    if int(topinfo[13]) < args.V_length:                  # 4. minimum alignment length >= 100
        return 'Alignment_length_shorter_than_cutoff'

    if float(topinfo[14]) / int(topinfo[13]) > 0.1:       # 5. mismatch ratio < 0.1
        return 'Mismatch_ratio_higher_than_0.1'

    frameinfo = topinfo[5].strip()
    if frameinfo.startswith('N') or frameinfo == '-':     # 3. need in-frame or out-of-frame info
        return 'No_frame_info'

    if topinfo[17] == topinfo[19] == topinfo[21]:
        if topinfo[16].split('*')[0] == topinfo[18].split('*')[0] == topinfo[20].split('*')[0]:
            return 'Top_3_hits_from_same_gene'
        else:
            return 'Top_3_hits_from_diff_gene'
    elif topinfo[17] == topinfo[19]:
        if topinfo[16].split('*')[0] == topinfo[18].split('*')[0]:
            return 'Top_2_hits_from_same_gene'
        else:
            return 'Top_2_hits_from_diff_gene'
    else:
        return 'Top_hit_is_unique'

def extract_igblast(sample, args):
    logging.info('Merge sample %s igblast results' % sample)
    eachdir = '%s/%s' % (args.outdir, sample)

    # check genome alignement if neccesary
    reads_align = {}
    if args.genomealign == 'T':
        for subset in ['unjoinR2', 'join']:
            for line in open('%s/%s_%s.sam' % (eachdir, sample, subset)):
                if line.startswith('@'): continue
                l = line.strip().split()
                if l[0] not in reads_align:
                    reads_align[l[0]] = [[l[2], l[3]]]
                else:
                    reads_align[l[0]].append([l[2], l[3]])

    fout = open('%s/%s.allread.xls' % (eachdir, sample), 'w')

    # extract best hit(s) from R2 file as a reference
    R2_tophits = {}
    R2_CDR12 = {}
    for line in open('%s/%s_unjoinR2.all.xls' % (eachdir, sample)):
        Vlist = []
        topinfo = line.strip().split('\t')
        if len(topinfo) < 10: continue
        if line.startswith('Qname'): continue
        if topinfo[16] == '-': continue
        Vlist.append(topinfo[16])
        if topinfo[17] == topinfo[19]:    # compare with the top score
            Vlist.append(topinfo[18])
        if topinfo[17] == topinfo[21]:    # compare with the top score
            Vlist.append(topinfo[20])
        if len(Vlist) >= 1:
            R2_tophits[topinfo[0]] = Vlist
            R2_CDR12[topinfo[0]] = topinfo[28: 32]

    # parse joined file
    for line in open('%s/%s_join.all.xls' % (eachdir, sample)):
        if line.startswith('Qname'):
            fout.write('%s\tSource\tStatus\tGene_name\tAnnotation\n' % line.strip())
        else:
            topinfo = line.strip().split('\t')
            if len(topinfo) < 10: continue
            hitcheck = hit_filter(topinfo, args)
            genename = topinfo[1].split('*')[0]

            if hitcheck in ['Top_3_hits_from_same_gene', 'Top_2_hits_from_same_gene', 'Top_hit_is_unique']:
                if args.genomealign == 'T':
                    nc = CheckAlignOverlap(topinfo, reads_align, Valign, args.genomealign, hitcheck)
                else:
                    nc = hitcheck
                if nc == hitcheck:
                    fout.write('%s\tjoin\tpass\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
                else:
                    fout.write('%s\tjoin\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, nc))
            else:
                fout.write('%s\tjoin\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))

    # parse R1
    for line in open('%s/%s_unjoinR1.all.xls' % (eachdir, sample)):
        if line.startswith('Qname'): continue
        topinfo = line.strip().split('\t')
        if len(topinfo) < 10: continue
        hitcheck = hit_filter(topinfo, args)
        genename = topinfo[1].split('*')[0]
        r = topinfo[0]
        if r in R2_CDR12:
            topinfo[28:32] = R2_CDR12[r]
        if r not in R2_tophits:
            hitcheck = hitcheck + '_R2_has_no_aligned_V'
            fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
        else:
            if hitcheck == 'Top_hit_is_unique':
                if topinfo[16] in R2_tophits[r]:
                    hitcheck = hitcheck + '_match_R2'
                    if args.genomealign == 'T':
                        nc = CheckAlignOverlap(topinfo, reads_align, Valign, args.genomealign, hitcheck)
                    else:
                        nc = hitcheck
                    if nc == hitcheck:
                        fout.write('%s\tR1\tpass\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
                    else:
                        fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, nc))

                else:
                    hitcheck = hitcheck + '_not_match_R2'
                    fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
            elif hitcheck == 'Top_2_hits_from_same_gene':
                if topinfo[16] in R2_tophits[r]:
                    if topinfo[18] in R2_tophits[r]:
                        hitcheck = hitcheck + '_match_R2'
                    else:
                        hitcheck = hitcheck + '_hit1_match_R2'
                    if args.genomealign == 'T':
                        nc = CheckAlignOverlap(topinfo, reads_align, Valign, args.genomealign, hitcheck)
                    else:
                        nc = hitcheck
                    if nc == hitcheck:
                        fout.write('%s\tR1\tpass\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
                    else:
                        fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, nc))
                elif topinfo[18] in R2_tophits[r]:
                    topinfo[1] = topinfo[18]
                    hitcheck = hitcheck + '_hit2_match_R2'
                    if args.genomealign == 'T':
                        nc = CheckAlignOverlap(topinfo, reads_align, Valign, args.genomealign, hitcheck)
                    else:
                        nc = hitcheck
                    if nc == hitcheck:
                        fout.write('%s\tR1\tpass\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
                    else:
                        fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, nc))
                else:
                    hitcheck = hitcheck + '_none_match_R2'
                    fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
            elif hitcheck == 'Top_3_hits_from_same_gene':
                if topinfo[16] in R2_tophits[r]:
                    if topinfo[18] in R2_tophits[r] and topinfo[20] in R2_tophits[r]:
                        hitcheck = hitcheck + '_match_R2'
                    elif topinfo[18] in R2_tophits[r]:
                        hitcheck = hitcheck + '_hit1_hit2_match_R2'
                    elif topinfo[20] in R2_tophits[r]:
                        hitcheck = hitcheck + '_hit1_hit3_match_R2'
                    else:
                        hitcheck = hitcheck + '_hit1_match_R2'
                    if args.genomealign == 'T':
                        nc = CheckAlignOverlap(topinfo, reads_align, Valign, args.genomealign, hitcheck)
                    else:
                        nc = hitcheck
                    if nc == hitcheck:
                        fout.write('%s\tR1\tpass\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
                    else:
                        fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, nc))
                elif topinfo[18] in R2_tophits[r]:
                    topinfo[1] = topinfo[18]
                    if topinfo[20] in R2_tophits[r]:
                        hitcheck = hitcheck + '_hit2_hit3_match_R2'
                    else:
                        hitcheck = hitcheck + '_hit2_match_R2'
                    if args.genomealign == 'T':
                        nc = CheckAlignOverlap(topinfo, reads_align, Valign, args.genomealign, hitcheck)
                    else:
                        nc = hitcheck
                    if nc == hitcheck:
                        fout.write('%s\tR1\tpass\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
                    else:
                        fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, nc))
                elif topinfo[20] in R2_tophits[r]:
                    topinfo[1] = topinfo[20]
                    hitcheck = hitcheck + '_hit3_match_R2'
                    if args.genomealign == 'T':
                        nc = CheckAlignOverlap(topinfo, reads_align, Valign, args.genomealign, hitcheck)
                    else:
                        nc = hitcheck
                    if nc == hitcheck:
                        fout.write('%s\tR1\tpass\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
                    else:
                        fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, nc))
                else:
                    hitcheck = hitcheck + '_none_match_R2'
                    fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
            else:
                fout.write('%s\tR1\tfailed\t%s\t%s\n' % ('\t'.join(topinfo), genename, hitcheck))
    fout.close()

    os.system('head -1 %s/%s.allread.xls > %s/%s.pass.xls' % (eachdir, sample, eachdir, sample))
    os.system('grep pass %s/%s.allread.xls >> %s/%s.pass.xls' % (eachdir, sample, eachdir, sample))

    # generate dedup results
    if args.dedup_by_seq:    # if dedup using seq
        fafile = '%s/%s_join.fa' % (eachdir, sample)    # load sequence info
        id_seq = {}
        id = ''
        for line in open(fafile):
            if line.startswith('>'):
                id = line.strip().replace('>', '')
                id_seq[id] = ''
            else:
                id_seq[id] += line.strip()

        checkseq_id = {}    # find longest read with uniq check seq
        tmplist = {}
        for line in open('%s/%s.pass.xls' % (eachdir, sample)):
            l = line.strip().split('\t')
            if line.startswith('Qname'): continue
            if l[-4] != 'join': continue
            newid = l[0]
            if newid not in id_seq: logging.error('Cannot find read %s in fasta file' % (l[0]))
            if len(id_seq[newid]) < args.dedup_by_seq: continue
            tmplist[newid] = 1
            checkseq = id_seq[newid][0: args.dedup_by_seq]
            if checkseq not in checkseq_id:
                checkseq_id[checkseq] = newid
            else:
                oldid = checkseq_id[checkseq]
                if len(id_seq[newid]) > len(id_seq[oldid]):
                    checkseq_id[checkseq] = newid

        uniquereads = checkseq_id.values()
        fdedup = open('%s/%s.dedup.xls' % (eachdir, sample), 'w')    # generate dedup file
        for line in open('%s/%s.pass.xls' % (eachdir, sample)):
            l = line.strip().split('\t')
            if line.startswith('Qname'):
                fdedup.write(line)
            else:
                if l[0] in uniquereads:
                    fdedup.write('%s\n' % line.strip())
        fdedup.close()
    else:

        uniquereads = {}   # dedup using CDR3 regions by parsing pass file col2-13
        fdedup = open('%s/%s.dedup.xls' % (eachdir, sample), 'w')
        for line in open('%s/%s.pass.xls' % (eachdir, sample)):
            l = line.strip().split('\t')
            if line.startswith('Qname'):
                fdedup.write('%s\n' % line.strip())
            else:
                tag = '|'.join(l[1: 13])
                if tag not in uniquereads:
                    uniquereads[tag] = 1
                    fdedup.write('%s\n' % line.strip())
        fdedup.close()

def parse_igblast(sample, subset, args):
    # subset : [join', 'unjoinR1', 'unjoinR2']
    topinfo = []
    summarytitle = ['Top V gene match', 'Top D gene match', 'Top J gene match', 'Chain type', 'V-J frame', 'Strand']
    germline = ['V end', 'V-D junction', 'D region', 'D-J junction', 'V-J junction', 'J start']
    top3hits = ['V_hit1', 'V_hit1_score', 'V_hit2', 'V_hit2_score', 'V_hit3', 'V_hit3_score',
                'D_hit1', 'D_hit1_score', 'D_hit2', 'D_hit2_score', 'D_hit3', 'D_hit3_score']
    eachdir = '%s/%s' % (args.outdir, sample)

    # output filenames
    fout = open('%s/%s_%s.all.xls' % (eachdir, sample, subset), 'w')
    fout.write('Qname\t%s\t%s\tTotal_V_alignment\tTotal_V_Mismatches\tTop_J_length\t%s\tCDR1_nucl\tCDR1_aa\tCDR2_nucl\tCDR2_aa\tCDR3_nucl\tCDR3_aa\n' % \
              ('\t'.join(summarytitle), '\t'.join(germline), '\t'.join(top3hits)) )

    # check file
    if not os.path.exists('%s/%s_%s.igblast' % (eachdir, sample, subset)):
        logging.error('Cannot find or read igblast result file %s_%s.igblast' % (sample, subset))
        sys.exit(-1)
    logging.info('Parsing igblast results %s_%s.igblast' % (sample, subset))

    # load read sequences
    reads_sequence = {}
    read = ''
    for line in open('%s/%s_%s.fa' % (eachdir, sample, subset)):
        if line.startswith('>'):
            read = line.strip().replace('>', '')
            reads_sequence[read] = ''
        else:
            reads_sequence[read] += line.strip()

    for line in open('%s/%s_%s.igblast' % (eachdir, sample, subset)):
        # init the parsing
        if line.startswith('# IGBLASTN 2'):
            # use to check whether there is length/mismatch info
            if topinfo != []:
                fout.write('%s\n' % '\t'.join(topinfo))
            topinfo = ['-' for x in range(34)]
            # init count number of top 3 V and D
            Vcount = 0
            Dcount = 0
        if line.startswith('# Query:'):
            qname = line.strip().replace('# Query: ','')
            topinfo[0] = qname
            cdr1_judge = [0, 0, 0]
            cdr2_judge = [0, 0, 0]
            reverse_judge = 'unknown'
        if line.startswith('#'): aboveline = line
        if line.strip() == '': continue

        l = line.strip().split('\t')
        if not line.startswith('#'):
            # parsing summary info
            if aboveline.startswith('# V-(D)-J rearrangement summary for query sequence'):
                items = aboveline.strip().split('(')[2].split(')')[0].split(',')
                l[0] = l[0].strip().split(',')[0]
                l[1] = l[1].strip().split(',')[0]
                l[2] = l[2].strip().split(',')[0]
                summary = {}
                for i in range(0, len(items)):
                    summary[items[i].strip()] = l[i]
                if summary['stop codon'] == 'Yes':
                    summary['V-J frame'] = summary['V-J frame'].replace('In-frame', 'In-frame with stop codon')
                topinfo[1: 7] = [summary.get(it, '-') for it in summarytitle]

            # parsing germline match
            if aboveline.startswith('# V-(D)-J junction details based on top germline gene matches'):
                items = aboveline.strip().split('(')[2].split(')')[0].split(',')
                gl = {}
                for i in range(0, len(items)):
                    gl[items[i].strip()] = l[i]
                topinfo[7: 13] = [gl.get(it, '-') for it in germline]

            # parsing CDR3 subregion
            if line.startswith('CDR3') and line.find('IMGT') == -1 and aboveline.startswith('# Sub-region sequence details'):
                topinfo[32] = l[1]
                if len(l) > 2:
                    topinfo[33] = l[2]
            # parsing match info
            if line.startswith('Total') and not line.startswith('Total queries'):
                topinfo[13: 15] = [l[3], l[5]]
            if line.startswith('FR1'):
                cdr1_judge = [1, 0, 0]
            if line.startswith('CDR1'):
                if cdr1_judge[0] == 1:
                    cdr1_judge[1] = int(l[1])
                    cdr1_judge[2] = int(l[2])
            if line.startswith('FR2'):
                cdr2_judge = [1, 0, 0]
            if line.startswith('CDR2'):
                if cdr2_judge[0] == 1:
                    cdr2_judge[1] = int(l[1])
                    cdr2_judge[2] = int(l[2])

            # parsing top3 V, D and J hits
            if aboveline.strip().endswith('hits found'):
                if line.startswith('V') and Vcount < 3:
                    p1 = 16 + Vcount * 2
                    p2 = 18 + Vcount * 2
                    topinfo[p1: p2] = [ l[2], l[-1] ]
                    Vcount += 1
                    if reverse_judge == 'unknown':
                        if line.find('reverse') == -1:
                            reverse_judge = 'n'
                            readseq = reads_sequence[topinfo[0]]
                        else:
                            reverse_judge = 'y'
                            readseq = reverse_complement(reads_sequence[topinfo[0]])
                        if cdr1_judge[1] > 1:
                            cdr1seq = readseq[cdr1_judge[1]-1: cdr1_judge[2]]
                            topinfo[28] = cdr1seq
                            topinfo[29] = str(Seq(cdr1seq, generic_dna).translate())
                        if cdr2_judge[1] > 1:
                            cdr2seq = readseq[cdr2_judge[1]-1: cdr2_judge[2]]
                            topinfo[30] = cdr2seq
                            topinfo[31] = str(Seq(cdr2seq, generic_dna).translate())

                if line.startswith('D') and Dcount < 3:
                    p1 = 22 + Dcount * 2
                    p2 = 24 + Dcount * 2
                    topinfo[p1: p2] = [ l[2], l[-1] ]
                    Dcount += 1
                if line.startswith('J') and l[2] == topinfo[3]:
                    topinfo[15] = l[4]
    fout.write('%s\n' % '\t'.join(topinfo))
    fout.close()

def run_igblast(sample, args):
    # use prepared databases
    if args.VDJdatabase == 'IGALL':
        Vdb = '-germline_db_V %s/database/database-imgt/%s_IGallV_imgt' % (scriptdir, args.organism)
        Ddb = '-germline_db_D %s/database/database-imgt/%s_IGallD_imgt' % (scriptdir, args.organism)
        Jdb = '-germline_db_J %s/database/database-imgt/%s_IGallJ_imgt' % (scriptdir, args.organism)
    elif args.VDJdatabase == 'TRALL':
        Vdb = '-germline_db_V %s/database/database-imgt/%s_TRallV_imgt' % (scriptdir, args.organism)
        Ddb = '-germline_db_D %s/database/database-imgt/%s_TRallD_imgt' % (scriptdir, args.organism)
        Jdb = '-germline_db_J %s/database/database-imgt/%s_TRallJ_imgt' % (scriptdir, args.organism)
    else:
        Vdb = '-germline_db_V %s/database/database-imgt/%s_%sV_imgt' % (scriptdir, args.organism, args.VDJdatabase)
        if args.mousestrain:
            Vdb =  Vdb + '_B6'
        Jdb = '-germline_db_J %s/database/database-imgt/%s_%sJ_imgt' % (scriptdir, args.organism, args.VDJdatabase)
        if args.VDJdatabase in ['IGH', 'TRB', 'TRD']:
            Ddb = '-germline_db_D %s/database/database-imgt/%s_%sD_imgt' % (scriptdir, args.organism, args.VDJdatabase)
            #if args.mousestrain:
            #    Ddb = Ddb + '_B6'
        elif args.VDJdatabase in ['IGK', 'IGL', 'TRG']:
            Ddb = '-germline_db_D %s/database/database/%s_gl_D' % (scriptdir, args.organism)
    # user customized databases
    if args.Vdb:
        Vdb = '-germline_db_V %s' % args.Vdb
    if args.Ddb:
        Ddb = '-germline_db_D %s' % args.Ddb
    if args.Jdb:
        Jdb = '-germline_db_J %s' % args.Jdb

    # user customized auxiliary file
    if args.auxiliary_data:
        aux = '-auxiliary_data %s' % args.auxiliary_data
    else:
        aux = '-auxiliary_data %s/database/optional_file/%s_gl.aux' % (scriptdir, args.organism)

    # run igblast
    if args.VDJdatabase.startswith('IG'):
        seqtype = 'Ig'
    if args.VDJdatabase.startswith('TR'):
        seqtype = 'TCR'
    for subset in ['join', 'unjoinR1', 'unjoinR2']:
        eachdir = '%s/%s' % (args.outdir, sample)
        cmdline = '%s/igblast_bin/igblastn -query %s/%s_%s.fa ' % (scriptdir, eachdir, sample, subset)
        cmdline += '-organism %s %s %s %s %s -ig_seqtype %s ' % (args.organism, Vdb, Ddb, Jdb, aux, seqtype)
        cmdline += '-domain_system %s ' % args.domain_system
        # igblast with tabular output
        cmdline_tabular = cmdline + ' -outfmt 7 -out %s/%s_%s.igblast' % (eachdir, sample, subset)
        os.environ['IGDATA'] = '%s/database/' % scriptdir
        os.environ['BLASTDB'] = '%s/database/' % scriptdir
        exec_log(cmdline_tabular)

        # run bowtie2
        genomefile = '%s/database/genomeseq/%s' % (scriptdir, args.chrseq)
        if args.genomealign == 'T':
            if subset == 'unjoinR2':
                cmdline_bowtie2 = 'bowtie2 -f -x %s -U %s/%s_%s.fa -S %s/%s_%s.sam --local --quiet' % (
                                    genomefile, eachdir, sample, subset, eachdir, sample, subset)
                exec_log(cmdline_bowtie2)
            if subset == 'join':
                cmdline_fatrim = 'fastx_trimmer -f {0} -i {1}/{2}_{3}.fa -o {1}/{2}_{3}.trim{0}.fa'.format(
                            args.trimforalign+1, eachdir, sample, subset)
                exec_log(cmdline_fatrim)
                cmdline_bowtie2 = 'bowtie2 -f -x %s -U %s/%s_%s.trim%d.fa -S %s/%s_%s.sam --local --quiet' % (
                                    genomefile, eachdir, sample, subset, args.trimforalign+1, eachdir, sample, subset)
                exec_log(cmdline_bowtie2)

def VDJ_alignment(si, args):
    global sample_info
    global scriptdir
    global Valign

    sample_info = si
    scriptdir = os.path.dirname(os.path.realpath(__file__)).replace('scripts', '')
    if args.genomealign == 'T':
        if args.Valignfile:
            Valign = load_Valign(args.Valignfile)
        else:
            Valign = load_Valign('%s/database/annotation/mouse_%s_Valign.txt' % (scriptdir, args.VDJdatabase))
    logging.info('Runing Igblast......')

    # perform igblast search
    if args.skipIgblast == 'F':
        if args.speedprocess == 'T':
            pool = multiprocessing.Pool(processes = len(sample_info) )
            for sample in sample_info:
                pool.apply_async(run_igblast, (sample, args, ))
            pool.close()
            pool.join()
        else:
            for sample in sample_info:
                run_igblast(sample, args)
    else:
        for sample in sample_info:
            if os.path.exists('%s/%s/igblast_raw' % (args.outdir, sample)):
                os.system('mv %s/%s/igblast_raw/* %s/%s/' % (args.outdir, sample, args.outdir, sample))
            if os.path.exists('%s/%s/igblast_results' % (args.outdir, sample)):
                os.system('mv %s/%s/igblast_results/* %s/%s/' % (args.outdir, sample, args.outdir, sample))
            if os.path.exists('%s/%s/reads_fasta/' % (args.outdir, sample)):
                os.system('mv %s/%s/reads_fasta/* %s/%s/' % (args.outdir, sample, args.outdir, sample))
            os.system('gunzip %s/%s/*fa.gz' % (args.outdir, sample))

    # parsing igblast results
    for sample in sample_info:
        for subset in ['join', 'unjoinR1', 'unjoinR2']:
            parse_igblast(sample, subset, args)
        extract_igblast(sample, args)
