#!/usr/bin/env python3
import os
import sys
import argparse
import logging

def parse_args():
    parser = argparse.ArgumentParser(description='The pipeline Performs HTGTS-Repertoire analysis.')
    subparser = parser.add_subparsers(help='commands to run pipeline',dest='subcmd')

    # run: execute the whole programd
    subp_run = subparser.add_parser('run',help='Run the whole program from fastq files.')
    # required arguments
    subp_run.add_argument("-r1", type = str, required = True, help = "MiSeq read 1 file" )
    subp_run.add_argument("-r2", type = str, required = True, help = "MiSeq read 2 file" )
    subp_run.add_argument("-m", dest = "metafile", type = str, required = True, help = "Metadata file documentation" )
    subp_run.add_argument("-o", dest = "outdir", type = str, required = True, help = "Output directory" )
    # optional arguments
    subp_run.add_argument("--organism", type = str, choices = ['mouse', 'human'], default = 'mouse', help = "Support mouse and human (mouse)" )
    subp_run.add_argument("--mousestrain", type = str, choices = ['B6'], help = "Use Black-6 mouse gene database or use pooled databases")
    subp_run.add_argument("--dedup_by_seq", type = int, help = "Remove duplicated reads based on sequence instead of CDR3 region, mutually exclusive from default dedup method of using col2-13 in pass.xls file" )
    subp_run.add_argument("--VDJdatabase", type = str, choices = ['IGH', 'IGK', 'IGL', 'TRA', 'TRB', 'TRG', 'TRD'], default = 'IGH', help = "Specify database for searching. ALL means pooled IG gene database or TR gene databases (IGH)" )
    subp_run.add_argument("--Vdb", type = str, help = "Specify Igblast germline V database." )
    subp_run.add_argument("--Ddb", type = str, help = "Specify Igblast germline D database." )
    subp_run.add_argument("--Jdb", type = str, help = "Specify Igblast germline J database." )
    subp_run.add_argument("--domain_system", choices = ['kabat'], default = 'kabat', help = "Domain system to be used for segment annotation (kabat)." )
    subp_run.add_argument("--auxiliary_data", type = str, help = "Specify file containing the coding frame start positions for sequences in germline J database." )
    subp_run.add_argument("--Vannotation", type = str, help = "Specify V gene annotation file which has three columns: V gene name, coordinates and function. " )
    subp_run.add_argument("--barcode_mismatch", type = int, default = 0, help = "N-maximum mismatch (0) to demultiplex" )
    subp_run.add_argument("--overlap", type = int, default = 10, help = "N-minimum overlap (10) to join reads" )
    subp_run.add_argument("--diffpercent", type = int, default = 8, help = "N-percent maximum difference (8) to join reads" )
    subp_run.add_argument("--fastq_quality_trimmer", type = int, default = 0, help = "Quality threshold - nucleotides with lower quality will be trimmed from the end of the sequence. (0)" )
    subp_run.add_argument("--genomealign", type = str, choices = ['F', 'T'], default = 'F', help = "Align reads to genome to help make judgement" )
    subp_run.add_argument("--chrseq", type = str, choices = ['mm10.chr12',], help = "Align reads to the chromosome" )
    subp_run.add_argument("--Valignfile", type = str, help = "Specify alignment results of V genes agaist chromossome defined by --chrseq" )
    subp_run.add_argument("--trimforalign", type = int, default = 100, help = "Trim (100) bp at 5' end on joined reads in alignment" )
    subp_run.add_argument("--V_score", type = int, default = 150, help = "Minimum Igblast score of V gene (150)" )
    subp_run.add_argument("--V_length", type = int, default = 100, help = "Minimum aligned length of V gene (100)" )
    subp_run.add_argument("--J_gene", type = str, help = "Reads failed in aligning to this J will be discard. eg: JH4" )
    subp_run.add_argument("--J_length", type = int, help = "Minimum alignment length of J gene. eg: 27" )
    subp_run.add_argument("--skipIgblast", type = str, choices = ['F', 'T'], default = 'F', help = "Skip Igblast and all upstream steps (F)" )
    subp_run.add_argument("--skipDemultiplex", type = str, choices = ['F', 'T'], default = 'F', help = "Skip Demultiplex and all upstream steps (F)" )
    subp_run.add_argument("--speedprocess", type = str, choices = ['F', 'T'], default = 'T', help = "Use maximum-allowed CPUs to run (F)" )

    # mutProfile: generate mutation profile along VDJ genes
    subp_mut = subparser.add_parser('mut',help='Generate mutation profile along VDJ genes. The log file in HTGTSrep result directory will be read, and same paramters will used in analysis.')
    subp_mut.add_argument("--dir", "-d", type = str, required = True, help = "HTGTSrep result directory" )
    subp_mut.add_argument("--dedup", type = str, default = "F", choices = ['T', 'F'], help = "Use de-duplicated reads (F)")
    subp_mut.add_argument("--joinonly", type = str, default = "T", choices = ['T', 'F'], help = "Use joined reads only (T)")
    subp_mut.add_argument("--geneseq", type = str, help = "Specify VDJ gene sequences (database/imgt_seq/*)" )
    subp_mut.add_argument("--genecdr", type = str, help = "Specify CDR1, 2, 3 start and end positions (database/imgt_seq/*)" )
    #subp_mut.add_argument("--additivesmooth", type = float, default = 0, help = "Use additive smoothing, 0 means no smoothing, bigger number the stronger of the smoothing (0)" )
    subp_mut.add_argument("--qscore", type = int, default = 20, help = "Base lower than this quality score will be marked as N (20)" )
    subp_mut.add_argument("--qscore_cov", type = int, default = 98, help = "Minimum percent of bases that must have [--qscore] quality (98)" )
    subp_mut.add_argument("--ymax_DNA", type = float, default = 0.75, help = "Maximum of Y axis in DNA profile image (0.75)" )
    subp_mut.add_argument("--ymax_protein", type = float, default = 0.3, help = "Maximum of Y axis in protein profile image (0.3)" )
    subp_mut.add_argument("--min_Vcov", type = float, default = 0.5, help = "Minimum coverage of V gene by the read (0.5)" )
    #subp_mut.add_argument("--min_readcov", type = int, default = 0, help = "Minimum coverage for position included in the profiling (0)" )
    subp_mut.add_argument("--min_readmap", type = int, default = 20, help = "Minimum aligned reads to create mutation profiling (20)" )
    subp_mut.add_argument("--mutreport", type = str, default = "F", help = 'Only generate a coverage and mutation frequency report (F)')

    # create mutProfile for multiple samples
    '''subp_re = subparser.add_parser('re',help='Remove duplicated reads')
    subp_re.add_argument("-m", dest = "metafile", type = str, required = True, help = "Metadata file documentation" )
    '''

    args = parser.parse_args()

    if args.subcmd == None:
        parser.print_help()
        sys.exit(0)
    return args

def check_customdatabase(type, path):
    if type == 'database':
        for suffix in ['.nhr', '.nin', '.nog', '.nsd', '.nsi', '.nsq']:
            filename = path + suffix
            if not os.path.exists(filename):
                logging.error('Cannot find or read database file %s' % filename)
                sys.exit(-1)
    elif type == 'auxiliary':
        if not os.path.exists(path):
            logging.error('Cannot find or read database file %s' % path)
            sys.exit(-1)

def check_metafile(args):
    # define global variabla, samplename: [barcode/MID, primer, adapter]e
    # Convert metafile to unix format
    try:
        os.system("perl -p -e 's/\\r/\\n/g' %s > %s/metadata.txt" % (args.metafile, args.outdir))
    except:
        logging.error('Problem converting line feeds to unix format')
        sys.exit(-1)

def load_metafile(metapath):
    # Parse metafile
    si = {}
    logging.info('Parsing meta files.....')
    for line in open(metapath):
        if line.startswith('Library'): continue
        l = line.split('\t')
        # check arguments
        if len(l) < 12: continue
        if len(l[11]) < 9:
            logging.warning('Warning: the primer is only %d bp' % len(l[11]))
        if l[11] == '':
            logging.error('Missing primer sequence of %s' % l[0])
            sys.exit(-1)
        if l[12] == '':
            logging.error('Missing adapter sequence of %s' % l[0])
            sys.exit(-1)
        try:
            samplename = '%s_%s' % (l[0], l[1])
            si[samplename] = [ l[10].upper(), l[11].upper(), l[12].upper() ]
        except:
            logging.error('Cannot find primer and adapter sequences in meta file')
            sys.exit(-1)
    return si

def check_args(args):
    # create output fold and define directory for logging
    logdir = ''
    if args.subcmd == "run":
        logdir = args.outdir + '/pipeline.log'
        if not os.path.exists(args.outdir):
            os.system('mkdir %s' % args.outdir)
    if args.subcmd == "mut":
        logdir = args.dir + '/pipeline_mut.log'
        if not os.path.exists(args.dir):
            print('Cannot find HTGTSrep result directory %s' % args.dir)
            sys.exit(-1)

    # init logging
    logging.basicConfig( level = 10,
    format = '%(levelname)-5s @ %(asctime)s: %(message)s ',
    datefmt = '%a, %d %b %Y %H:%M:%S',
    filename = logdir,
    filemode = 'a' )
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(levelname)-5s @ %(asctime)s: %(message)s ','%a, %d %b %Y %H:%M:%S')
    #formatter.formatTime('%a, %d %b %Y %H:%M:%S')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    # add paramters
    print('\nWelcome to HTGTSrep pipeline!!! ')
    logging.info('Parameters: '+' '.join(sys.argv))

    # check run subcommand arguments
    if args.subcmd == 'run':
        # check neccesary config
        #if not "IGDATA" in os.environ or not "BLASTDB" in os.environ:
        #    logging.error("Cannot find environment variable IGDATA for igblast\nPlease the following config in your .bashrc or .bash_profile\nexport IGDATA=/usr/share/igblast_database\nexport BLASTDB=/usr/share/igblast_database/database-imgt:/usr/share/igblast_database/database")
        #    sys.exit(-1)
        if not os.path.exists(args.r1):
            logging.error('Cannot find or read read 1 file %s' % args.r1)
            sys.exit(-1)
        if not os.path.exists(args.r2):
            logging.error('Cannot find or read read 2 file %s' % args.r2)
            sys.exit(-1)

        scriptdir = os.path.dirname(os.path.realpath(__file__)).replace('scripts', '')
        if not os.path.exists('%s/igblast_bin/igblastn' % scriptdir):
            logging.error('Cannot find igblastn program at igblast_bin/')
            sys.exit(-1)
        if not os.path.exists('%s/database/internal_data' % scriptdir):
            logging.error('Cannot find igblast-required internal_data')
            sys.exit(-1)

        if not os.path.exists(args.metafile):
            logging.error('Cannot find or read meta file %s' % args.metafile)
            sys.exit(-1)
        check_metafile(args)
        si = load_metafile("%s/metadata.txt" % args.outdir)

        if args.dedup_by_seq and args.dedup_by_seq < 100:
            logging.warning('Over-dedup can happen by only using %d bps' % args.dedup_by_seq)
        if args.auxiliary_data:
            check_customdatabase('auxiliary', args.auxiliary_data)
        if args.Vdb:
            check_customdatabase('database', args.Vdb)
            if (not args.Valignfile) and args.genomealign == 'T':
                logging.error('Must specify V gene alignment file for %s' % args.Vdb)
                sys.exit(-1)
        if args.Ddb:
            check_customdatabase('database', args.Ddb)
        if args.Jdb:
            check_customdatabase('database', args.Jdb)
        if args.Vannotation:
            if not os.path.exists(args.Vannotation):
                logging.error('Cannot find or read V gene annotation file %s' % args.Vannotation)
                sys.exit(-1)
        if args.mousestrain:
            if args.organism == 'human':
                logging.error('Cannot specify mouse strain in human')
                sys.exit(-1)
            if args.VDJdatabase not in ['IGH', 'IGK']:
                logging.error('Only support Black-6 of IGH or IGK')
                sys.exit(-1)
        if args.fastq_quality_trimmer < 0:
            logging.error('Cannot accept < 0 quality score')
        if args.genomealign == 'T' and (not args.chrseq):
            logging.error('Must specify one chromosome to do alignment')
            sys.exit(-1)
        if args.chrseq or args.Valignfile:
            args.genomealign = 'T'
        if args.Valignfile:
            if not os.path.exists(args.Valignfile):
                logging.error('Cannot find or read V alignment file %s' % args.Valignfile)
                sys.exit(-1)
        return si

    # check mut subcommand arguments
    if args.subcmd == 'mut':
        if not os.path.exists('%s/pipeline.log' % args.dir):
            logging.error('Cannot find or read log file %s/pipeline.log' % args.dir)
            sys.exit(-1)
        metapath = '%s/metadata.txt' % args.dir
        if args.geneseq and not os.path.exists(args.geneseq):
            logging.error('Cannot find or read gene sequence file %s' % args.geneseq)
            sys.exit(-1)
        if args.genecdr and not os.path.exists(args.genecdr):
            logging.error('Cannot find or read CDR information file %s' % args.genecdr)
            sys.exit(-1)
        if not os.path.exists(metapath):
            logging.error('Cannot find or read meta file %s' % metapath)
            sys.exit(-1)
        si = load_metafile(metapath)
        #if args.additivesmooth < 0:
        #    logging.error('Smoothing parameter must > 0')
        #    sys.exit(-1)
        if args.qscore > 30:
            logging.warning('You set a stringent qscore cutoff!')
        if args.ymax_DNA <= 0 or args.ymax_protein <= 0:
            logging.error('Cannot use negative value in Y axis')
            sys.exit(-1)

        for sample in si:
            for t in ['join', 'unjoinR1', 'unjoinR2']:
                p = '%s/%s/reads_fasta/%s_%s.fa.gz' % (args.dir, sample, sample, t)
                if not os.path.exists(p):
                    logging.error('Cannot find or read fasta file %s' % p)
            if args.dedup == 'T':
                p = '%s/%s/%s.dedup.xls' % (args.dir, sample, sample)
            else:
                p = '%s/%s/%s.pass.xls' % (args.dir, sample, sample)
            if not os.path.exists(p):
                logging.error('Cannot find or read file %s' % p)
            if not os.path.exists('%s/%s/mutProfile' % (args.dir, sample)):
                os.system('mkdir %s/%s/mutProfile' % (args.dir, sample))
        return si
