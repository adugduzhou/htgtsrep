#!/usr/bin/env python3
import random
import os
import sys
import logging

def load_Valign(fname):
    V_align = {}
    for line in open(fname):
        l = line.strip().split()
        start_end = '%s_%s' % (l[2], l[3])
        if l[0] not in V_align:
            V_align[l[0]] = {l[1]: [start_end]}
        else:
            if l[1] in V_align[l[0]]:
                V_align[l[0]][l[1]].append(start_end)
            else:
                V_align[l[0]][l[1]] = [start_end]
    return V_align

def CheckAlignOverlap(topinfo, reads_align, Valign, genomealign, hitcheck):
    flag = 'noneed'
    if genomealign == 'T':
        flag = 'unmatch'
        if topinfo[0] not in reads_align:
            flag = 'nohit'
        else:
            for loc in reads_align[topinfo[0]]:
                chrom = loc[0]
                pos = int(loc[1])
                if topinfo[1] not in Valign:
                    flag = 'noVhit'
                    continue
                if chrom in Valign[topinfo[1]]:
                    for start_end in Valign[topinfo[1]][chrom]:
                        start = int(start_end.split('_')[0])
                        end = int(start_end.split('_')[1])
                        # extend 10bp at 5' because V-D or V-J junctions might have matches
                        if (start - 10) <= pos <= end:
                            flag = 'match'
    if flag == 'nohit':
        return 'No_hit_from_genome_alignment'
    elif flag == 'noVhit':
        return 'topVgene_has_no_alignment'
    elif flag == 'unmatch':
        return 'genome_alignment_unmatch_Vgene'
    else:
        return hitcheck

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def exec_log(cmdline):
    logging.info(cmdline)
    os.system(cmdline)

def line_count(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def reads_stat(args):
    for sample in sample_info:
        eachdir = '%s/%s' % (args.outdir, sample)
        fstat = open('%s/%s.stat.txt' % (eachdir, sample), 'w')
        # reads count
        total_num = line_count("%s/%s_R1.fq" % (eachdir, sample)) / 4
        join_num = line_count("%s/%s_join.fq" % (eachdir, sample)) / 4
        unjoin_num = total_num - join_num
        fstat.write('Total reads\t%d\nJoined reads\t%d\nUnjoined reads\t%d\n' % (total_num, join_num, unjoin_num))

        # alignment stat
        join_uniq = line_count('%s/%s_join.uniq.xls' % (eachdir, sample))
        R1_uniq = line_count('%s/%s_unjoinR1.uniq.xls' % (eachdir, sample))
        join_NOuniq = line_count('%s/%s_join.NOuniq.xls' % (eachdir, sample))
        R1_NOuniq = line_count('%s/%s_unjoinR1.NOuniq.xls' % (eachdir, sample))
        mergeNum = line_count('%s/%s.Igblast_merge.xls' % (eachdir, sample))
        fstat.write('# of uniquely/NON-uniquely joined hits\t%d\t%d\n' % (join_uniq, join_NOuniq))
        fstat.write('# of uniquely/NON-uniquely unjoined-R1 hits\t%d\t%d\n' % (R1_uniq, R1_NOuniq))
        fstat.write('# of merged hits\t%d\n' % mergeNum)
        fstat.close()

def random_seq(length):
    ''' Generate random sequnce with input length '''
    seq = ''
    if length == 0:
        return seq
    else:
        seq = ''.join([random.choice('ATCG') for i in range(0, length)])
    return seq

def mutate_seq(orig_string, mutation_rate=0.005):
    ''' Mutate input sequence with point mutations '''
    bases = "ACGT"
    result = []
    mutations = []
    n = 0
    for base in orig_string:
        n += 1
        if random.random() < mutation_rate and base in bases:
            new_base = bases[bases.index(base) - random.randint(1, 3)] # negatives are OK
            result.append(new_base)
            mutations.append('%s%d%s' % (base, n, new_base))
        else:
            result.append(base)
    return "".join(result), '|'.join(mutations)

def reverse_complement(seq):
    complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}
    seq_rc = "".join(complement.get(base, base) for base in reversed(seq))
    return seq_rc

def fastq_stats(fastqfile):
    # execute fastq-stats
    os.system('fastq-stats %s > %s.fastqstat' % (fastqfile, fastqfile))
    # parse results
    fqstat = {}
    for line in open('%s.fastqstat' % fastqfile):
        l = line.strip().split('\t')
        fqstat[l[0]] = l[1]
    os.system('rm -rf %s.fastqstat' % fastqfile)
    return fqstat

def parsefa_long(file, length):
    id_seq = {}
    id = ''
    for line in open(file):
        if line.startswith('>'):
            id = line.strip()
            id_seq[id] = ''
        else:
            id_seq[id] += seq
    fout = open(file.replace('.fa', '.long.fa'), 'w')
    for id in id_seq:
        if len(id_seq[id]) >= length:
            fout.write('%s\n%s\n' % (id, id_seq[id]))
    fout.close()

def smooth(self, nucMutnum, nucCovnum, genetotalseq, statfilelist):
    #print(nucMutnum['A'], nucMutnum['G'], nucMutnum['C'], nucMutnum['T'])
    #print(nucCovnum['A'], nucCovnum['G'], nucCovnum['C'], nucCovnum['T'])
    nucMucratio = {}
    smoothpower = self.args.additivesmooth
    for nuc in 'AGCT':
        nucMucratio[nuc] = float(nucMutnum[nuc]) / nucCovnum[nuc]
    avecover = sum([nucCovnum[a] for a in 'AGCT']) / len(genetotalseq)
    for gene in statfilelist:
        statfile = statfilelist[gene]
        statnew = statfile.replace('.txt', '.sm%s.txt' % str(smoothpower))
        fnew = open(statnew, 'w')
        for line in open(statfile):
            if line.startswith('Pos'):
                fnew.write(line)
            else:
                l = line.strip().split('\t')
                total_smooth = int(l[2]) + avecover * smoothpower
                mut_smooth = int(l[1]) + nucMucratio[nuc] * avecover * smoothpower
                if total_smooth == 0:
                    l[4] = 0
                else:
                    l[4] = mut_smooth / total_smooth
                l[4] = str(l[4])
                fnew.write('%s\n' % '\t'.join(l))
        fnew.close()
        pdffile = statnew.replace('nucfile', 'profiles').replace('txt', 'pdf')
        exec_log('Rscript scripts/SHMPlot2.R %s %s plotrows=1 figureheight=2 showsequence=FALSE ymax=0.2 cdr1_start=%d cdr1_end=%d cdr2_start=%d cdr2_end=%d cdr3_start=%d cdr3_end=%d' % \
                (statnew, pdffile, self.V_CDR[gene]['CDR1_start'], self.V_CDR[gene]['CDR1_end'], \
                self.V_CDR[gene]['CDR2_start'], self.V_CDR[gene]['CDR2_end'], \
                self.V_CDR[gene]['CDR3_start'], self.V_CDR[gene]['CDR3_end']))

'''def CDR_cal(self, querystart, current_CDR, Vgene):
    for type in current_CDR:
        if current_CDR[type] != 0:
            pos = querystart + current_CDR[type] - 1
            if self.V_CDR[Vgene][type] == 0:
                self.V_CDR[Vgene][type] = pos
            if type.endswith('start') and pos < self.V_CDR[Vgene][type]:
                self.V_CDR[Vgene][type] = pos
            if type.endswith('end') and pos > self.V_CDR[Vgene][type]:
                self.V_CDR[Vgene][type] = pos'''
