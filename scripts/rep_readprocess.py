#!/usr/bin/env python3
import multiprocessing
import os, sys
import logging
from scripts.rep_lib import *

def sample_demultiplex(args):
    # create barcode file
    fbar = open('%s/barcodes.txt' % args.outdir, 'w')
    for sample in sample_info:
        barcode = (sample_info[sample][0] + sample_info[sample][1])[:12]
        fbar.write('%s\t%s\n' % (sample, barcode))
    fbar.close()

    # create directories if not exist
    for sample in sample_info:
        eachdir = '%s/%s' % (args.outdir, sample)
        if not os.path.exists(eachdir):
            os.system('mkdir %s' % eachdir)

    # gunzip raw fastq if gzipped
    if args.skipDemultiplex == 'F':
        if (args.r1).endswith('gz') or (args.r1).endswith('gzip'):
            #os.system('gzip -c -d %s | fastq_quality_trimmer -t 10 > %s/raw.r1.fq' % (args.r1, args.outdir))
            #os.system('gzip -c -d %s | fastq_quality_trimmer -t 10 > %s/raw.r2.fq' % (args.r2, args.outdir))
            os.system('gzip -c -d %s > %s/raw.r1.fq' % (args.r1, args.outdir))
            os.system('gzip -c -d %s > %s/raw.r2.fq' % (args.r2, args.outdir))
        else:
            #os.system('cat %s | fastq_quality_trimmer -t 10 > %s/raw.r1.fq' % (args.r1, args.outdir))
            #os.system('cat %s | fastq_quality_trimmer -t 10 > %s/raw.r2.fq' % (args.r2, args.outdir))
            os.system('cp %s %s %s' % (args.r1, args.r2, args.outdir))
        # demultiplex
        try:
            cmdline = ('fastq-multx -m %d -x -b -d 0 -B %s/barcodes.txt %s/raw.r1.fq %s/raw.r2.fq -o %s/%%_R1.fq %s/%%_R2.fq' % (
                        args.barcode_mismatch, args.outdir, args.outdir, args.outdir, args.outdir, args.outdir))
            exec_log(cmdline)
        except:
            logging.error('Cannot run fastq-multx to demultiplex samples\n')
            sys.exit(-1)

        # clean raw data and move files to diff fold
        for sample in sample_info:
            os.system('mv %s/%s_R* %s/%s' % (args.outdir, sample, args.outdir, sample))
        os.system('mkdir %s/unmatched' % args.outdir)
        os.system('mv %s/unmatched_* %s/unmatched' % (args.outdir, args.outdir))
        os.system('rm -rf %s/raw.r1.fq %s/raw.r2.fq' % (args.outdir, args.outdir))

def sample_trim(sample, args):
    eachdir = '%s/%s' % (args.outdir, sample)
    barcode = sample_info[sample][0]
    primer = sample_info[sample][1]
    primertrim = reverse_complement((barcode + primer))[:10]
    adapter = sample_info[sample][2].strip()
    adapter_rc = reverse_complement(adapter).strip()

    # 1. trim adapter at R2 5' end and seq after primer at R2 3' end
    try:
        exec_log('cutadapt --quiet -g %s -a %s -n 2 -m 50 -o %s/%s_R2.trim.fq %s/%s_R2.fq' % (adapter_rc.strip(), primertrim, eachdir, sample, eachdir, sample))
    except:
        logging.error('Cannot run cutadapt to trim adapter')
        sys.exit(-1)

    # 2. trim barcode at R1 5' end adapter at R1 3' end
    if barcode != '':
        exec_log('cutadapt --quiet -g ^%s -a %s -n 2 -m 50 -o %s/%s_R1.trim.fq %s/%s_R1.fq' % (barcode, adapter, eachdir, sample, eachdir, sample))
    else:
        exec_log('cutadapt --quiet -m 50 -a %s -o %s/%s_R1.trim.fq %s/%s_R1.fq' % (adapter, eachdir, sample, eachdir, sample))

    # 3. join R1 and R2 fastq files, and convert to fasta files
    exec_log("awk '{{print $1}}' {0}/{1}_R1.trim.fq > {0}/{1}_R1.clean.fq".format(eachdir, sample))
    exec_log("awk '{{print $1}}' {0}/{1}_R2.trim.fq > {0}/{1}_R2.clean.fq".format(eachdir, sample))
    exec_log("awk '{{ if(NR % 4 == 1) {{print $1}} }}' {0}/{1}_R1.clean.fq | sed 's/@//' | sort > {0}/{1}_R1.reads.list".format(eachdir, sample))
    exec_log("awk '{{ if(NR % 4 == 1) {{print $1}} }}' {0}/{1}_R2.clean.fq | sed 's/@//' | sort > {0}/{1}_R2.reads.list".format(eachdir, sample))
    exec_log('comm -12 {0}/{1}_R1.reads.list {0}/{1}_R2.reads.list > {0}/{1}_comm.reads.list'.format(eachdir, sample))
    exec_log('seqtk subseq {0}/{1}_R1.clean.fq {0}/{1}_comm.reads.list > {0}/{1}_R1.even.fq'.format(eachdir, sample))
    exec_log('seqtk subseq {0}/{1}_R2.clean.fq {0}/{1}_comm.reads.list > {0}/{1}_R2.even.fq'.format(eachdir, sample))
    try:
        exec_log('fastq-join -p %d -m %d %s/%s_R1.even.fq %s/%s_R2.even.fq -o %s/%s_unjoinR1.fq -o %s/%s_unjoinR2.fq -o %s/%s_join.fq' % (args.diffpercent, args.overlap, eachdir, sample, eachdir, sample, eachdir, sample, eachdir, sample, eachdir, sample))
    except:
        logging.error('Cannot run fastq-join to join read1 and read2')
        sys.exit(-1)

    # 4. Convert fq to fa and keep long reads
    for subset in ['join', 'unjoinR1', 'unjoinR2']:
        prefix = "%s/%s_%s" % (eachdir, sample, subset)
        try:
            if subset == 'unjoinR1':
                if args.fastq_quality_trimmer > 0:
                    exec_log("fastq_quality_trimmer -t %d -i %s.fq -Q33 | fastq_to_fasta -Q33 -n | awk '{print $1}' > %s.fa" % (args.fastq_quality_trimmer, prefix, prefix))
                else:
                    exec_log("fastq_to_fasta -i %s.fq -Q33 -n | awk '{print $1}' > %s.fa" % (prefix, prefix))
            else:
                exec_log("fastq_to_fasta -i %s.fq -Q33 -n | awk '{print $1}' > %s.fa" % (prefix, prefix))
        except:
            logging.error('Cannot run FASTX-Toolkit to convert fq to fa format')
            sys.exit(-1)

def reads_process(si, args):
    global sample_info
    sample_info = si

    # 1. demultiplex samples
    logging.info('Parsing reads files.....')
    logging.info('Demultiplexing reads......')
    sample_demultiplex(args)

    # 2. trim adapter and junk sequences & join R1 and R2 reads
    logging.info('Joining reads......')
    '''if len(sample_info) > 10:
        pcore = 10
    else:
        pcore = len(sample_info)'''
    pool = multiprocessing.Pool(processes = len(sample_info) )
    for sample in sample_info:
        pool.apply_async(sample_trim, (sample, args, ))
    pool.close()
    pool.join()
