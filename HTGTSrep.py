#!/usr/bin/env python3

'''HTGTS Repertoire Pipeline
Copyright (c) 2016 Zhou Du, Fred Alt lab
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD License (see the file COPYING included with
the distribution).
@version: experimental
@author:  Zhou Du
'''

import os
import sys
import time
import argparse
import subprocess
import multiprocessing
import logging

scriptdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(scriptdir)

from scripts.rep_argparser import *
from scripts.rep_readprocess import *
from scripts.rep_igblast import *
from scripts.rep_stat import *
from scripts.rep_mutParser import *

# main function
def main():
    args = parse_args()
    SamplesInfo = check_args(args)

    if args.subcmd == 'run':
        if args.skipIgblast == 'F':
            reads_process(SamplesInfo, args)
        VDJ_alignment(SamplesInfo, args)
        get_statistic(SamplesInfo, args)

    if args.subcmd == 'mut':
        mutProfile(SamplesInfo, args)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        sys.stderr.write("User interrupt me! ;-) Bye!\n")
        sys.exit(0)
